import 'package:flutter/material.dart';

import 'screens/home_screen.dart';
import 'screens/tabs_screen.dart';
import 'screens/login_screen.dart';
import 'screens/register_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "KonvaSearch",
      theme: ThemeData(
        primaryColor: Color.fromRGBO(220, 234, 249, 1),
        accentColor: Color.fromRGBO(204, 23, 40, 1),
        fontFamily: "Roboto",
        textTheme: ThemeData.light().textTheme.copyWith(
          headline1: TextStyle(
            color: Color.fromRGBO(255, 0, 0, 1),
            fontSize: 40,
            fontFamily: 'Roboto',
          ),
          headline2: TextStyle(
            color: Color.fromRGBO(204, 23, 40, 1),
            fontSize: 40,
            fontFamily: 'Roboto',
          ),
          bodyText1: TextStyle(
            fontSize: 30,
            fontFamily: 'Roboto',
          ),
        )
      ),
      // home: TabsScreen(),
      initialRoute: '/',
      routes: {
        '/': (ctx) => TabsScreen(),
        LoginScreen.routeName: (ctx) => LoginScreen(),
        RegisterScreen.routeName: (ctx) => RegisterScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => HomeScreen(),
        );
      },
    );
  }
}