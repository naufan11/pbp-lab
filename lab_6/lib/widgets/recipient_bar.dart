import 'package:flutter/material.dart';

class RecipientBar extends StatefulWidget {
  @override
  _RecipientBarState createState() => _RecipientBarState();
}

class _RecipientBarState extends State<RecipientBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const LinearProgressIndicator(
          value: 0.2,
          color: Color.fromRGBO(204, 23, 40, 1),
          backgroundColor: Color.fromRGBO(235, 235, 235, 1),
          minHeight: 20,
        ),
        SizedBox(height: 5),
        RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "2",
                style: TextStyle(
                  color: Color.fromRGBO(204, 23, 40, 1),
                  fontFamily: 'Roboto',
                  fontSize: 20,
                ),
              ),
              TextSpan(
                text: " orang sedang mencari donor plasma",
                style: TextStyle(
                  color: Color.fromRGBO(32, 59, 97, 1),
                  fontFamily: 'Roboto',
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}