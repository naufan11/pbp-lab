import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

final List<String> carouselData = [
  "Apa itu konvalesen?",
  "Bagaimana cara kerjanya?",
];

class Carousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      width: double.infinity,
      child: CarouselSlider(
          options: CarouselOptions(
          autoPlay: true,
          aspectRatio: 2.0,
          enlargeCenterPage: true,
        ),
        items: imageSliders,
      ),
    );
  }
}


// final List<Widget> imageSliders = carouselData
//     .map((item) => Container(
//           child: Container(
//             margin: EdgeInsets.all(5.0),
//             child: Text(
//               item,
//               style: TextStyle(
//                 fontSize: 40,
//               ),
//               textAlign: TextAlign.center,
//             ),
//           ),
//         ))
//     .toList();

final List<Widget> imageSliders = carouselData
    .map((item) => 
      Container(
        margin: EdgeInsets.all(5.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Container(
            width: double.infinity,
            height: 150,
            padding: EdgeInsets.all(20.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  Text(
                    item,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 30,
                      fontFamily: 'Roboto',
                      color: Color.fromRGBO(32, 59, 97, 1),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  const Divider(
                    color: Color.fromRGBO(32, 59, 97, 1),
                    height: 15,
                    thickness: 1,
                    indent: 50,
                    endIndent: 50,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    )
    .toList();