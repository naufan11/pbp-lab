import 'package:flutter/material.dart';

class DonorBar extends StatefulWidget {
  @override
  _DonorBarState createState() => _DonorBarState();
}

class _DonorBarState extends State<DonorBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const LinearProgressIndicator(
          value: 0.2,
          color: Color.fromRGBO(255, 0, 0, 1),
          backgroundColor: Color.fromRGBO(235, 235, 235, 1),
          minHeight: 20,
        ),
        SizedBox(height: 5),
        RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "2",
                style: TextStyle(
                  color: Color.fromRGBO(255, 0, 0, 1),
                  fontFamily: 'Roboto',
                  fontSize: 20,
                ),
              ),
              TextSpan(
                text: " orang ingin mendonorkan plasmanya",
                style: TextStyle(
                  color: Color.fromRGBO(32, 59, 97, 1),
                  fontFamily: 'Roboto',
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}