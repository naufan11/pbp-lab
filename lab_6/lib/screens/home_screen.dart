import 'package:flutter/material.dart';

import '../widgets/donor_bar.dart';
import '../widgets/recipient_bar.dart';
import '../widgets/carousel.dart';

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Color.fromRGBO(210, 224, 239, 1),
          child: Column(
            children: [
              SizedBox( height: 10, ),
              Carousel(),
              SizedBox( height: 10, ),
            ],
          ),
        ),
        Spacer(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Text(
                "Status saat ini",
                style: TextStyle(
                  fontSize: 40,
                  color: Color.fromRGBO(32, 59, 97, 1),
                ),
              ),
              SizedBox(height: 10,),
              DonorBar(),
              SizedBox(height:30),
              RecipientBar(),
            ],
          ),
        ),
        Spacer(),
      ],
    );
  }
}