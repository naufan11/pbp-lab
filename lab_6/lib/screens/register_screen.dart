import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:email_validator/email_validator.dart';

class RegisterScreen extends StatefulWidget {
  static const routeName = '/register';
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _registerFormKey = GlobalKey<FormState>();
  bool isPasswordVisible = false;

  void togglePasswordView() {
    setState(() {
      isPasswordVisible = !isPasswordVisible;
    });
  }

  final myController = TextEditingController();
  String email = "";
  String username = "";
  String password1 = "";
  String password2 = "";

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: <Widget>[
            const SizedBox( height: 60, ),
            const Image(
              image: AssetImage('assets/images/logo.png'),
              width: 100,
              height: 100,
            ),
            const SizedBox( height: 30, ),

            // Form
            Form(
              key: _registerFormKey,
              child: Container(
                child: Column(
                  children: [
                    // Email ===================================================
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(250, 250, 250, 0.95),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: TextFormField(
                        onChanged: (String value) {
                          email = value;
                        },
                        decoration: InputDecoration(
                          hintText: 'Email',
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(200, 200, 200, 1),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none, 
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Email tidak boleh kosong";
                          } else if (!EmailValidator.validate(value)) {
                            return "Email tidak valid";
                          }
                          return null;
                        },
                      )
                    ),
                    SizedBox( height: 20, ),
                    // Username ================================================
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(250, 250, 250, 0.95),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: TextFormField(
                        onChanged: (String value) {
                          username = value;
                        },
                        decoration: InputDecoration(
                          hintText: 'Username',
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(200, 200, 200, 1),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none, 
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Username tidak boleh kosong";
                          }
                          return null;
                        },
                      )
                    ),
                    SizedBox( height: 20, ),
                    // Password ================================================
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(250, 250, 250, 0.95),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: TextFormField(
                        onChanged: (String value) {
                          password1 = value;
                        },
                        obscureText: !isPasswordVisible,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(200, 200, 200, 1),
                          ),
                          suffixIcon: IconButton(
                            color: Color.fromRGBO(200, 200, 200, 1),
                            splashRadius: 1,
                            icon: Icon(isPasswordVisible ? 
                              Icons.visibility_outlined : 
                              Icons.visibility_off_outlined
                            ),
                            onPressed: togglePasswordView,
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none, 
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Password tidak boleh kosong";
                          }
                          return null;
                        },
                      )
                    ),
                    SizedBox( height: 20, ),
                    // Confirm Password ========================================
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(250, 250, 250, 0.95),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: TextFormField(
                        onChanged: (String value) {
                          password2 = value;
                        },
                        obscureText: !isPasswordVisible,
                        decoration: InputDecoration(
                          hintText: 'Confirm Password',
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(200, 200, 200, 1),
                          ),
                          suffixIcon: IconButton(
                            color: Color.fromRGBO(200, 200, 200, 1),
                            splashRadius: 1,
                            icon: Icon(isPasswordVisible ? 
                              Icons.visibility_outlined : 
                              Icons.visibility_off_outlined
                            ),
                            onPressed: togglePasswordView,
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none, 
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Password tidak boleh kosong";
                          } else if (value != password1) {
                            return "Password tidak sama";
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 30,),
                    // Register Button =========================================
                    Container(
                      width: double.infinity,
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                            Color.fromRGBO(255, 0, 0, 1)
                          ),
                          foregroundColor: MaterialStateProperty.all<Color>(
                            Colors.white
                          ),
                          overlayColor: MaterialStateProperty.resolveWith<Color?>(
                            (Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed))
                                return Color.fromRGBO(204, 23, 40, 1);
                              return null;
                            }
                          ),
                        ),
                        onPressed: () {
                          if (_registerFormKey.currentState!.validate()) {
                            ScaffoldMessenger.of(context).showSnackBar(
                               const SnackBar(content: Text('Processing Data')),
                            );
                            print(email);
                            print(username);
                            print(password1);
                            print(password2);
                          } else {
                            print("Ga valid");
                          }
                        },
                        child: Text(
                          "Register",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox( height: 10, ),
            RichText(
              text: TextSpan(
                text: "Sudah memiliki akun?",
                style: TextStyle(
                  color: Colors.black,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.popAndPushNamed(context, "/login");
                  }
              ),
            ),
          ],
        ),
      )
    );
  }
}