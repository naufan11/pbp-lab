import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

class AccountScreen extends StatelessWidget {
  static const routeName = '/account';
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        // SizedBox( height: 20, ),
        Spacer(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          width: double.infinity,
          child: TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                Color.fromRGBO(204, 23, 40, 1)
              ),
              foregroundColor: MaterialStateProperty.all<Color>(
                Colors.white
              ),
              overlayColor: MaterialStateProperty.resolveWith<Color?>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed))
                    return Color.fromRGBO(255, 0, 0, 1);
                  return null; // Defer to the widget's default.
                }
              ),
            ),
            onPressed: () {
              Navigator.pushNamed(context, "/login");
            },
            child: Text(
              "Login",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          width: double.infinity,
          child: TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                Color.fromRGBO(255, 0, 0, 1)
              ),
              foregroundColor: MaterialStateProperty.all<Color>(
                Colors.white
              ),
              overlayColor: MaterialStateProperty.resolveWith<Color?>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed))
                    return Color.fromRGBO(204, 23, 40, 1);
                  return null; // Defer to the widget's default.
                }
              ),
            ),
            onPressed: () {
              Navigator.pushNamed(context, "/register");
            },
            child: Text(
              "Register",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ),
        Spacer(),
      ],
    );
  }
}