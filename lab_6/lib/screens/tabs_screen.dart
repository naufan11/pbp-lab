import 'package:flutter/material.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';


import '../widgets/main_drawer.dart';

import './home_screen.dart';
import './login_screen.dart';
import './register_screen.dart';
import './account_screen.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  int _selectedPageIndex = 0;

  final List<Map<String, Widget>> _pages = [
    {
      'page': HomeScreen(),
      'title': RichText(
        text: TextSpan(
          children: <TextSpan>[
            TextSpan(
              text: "Konva", 
              style: TextStyle(
                color: Color.fromRGBO(255, 0, 0, 1),
                fontSize: 40,
              )
            ),
            TextSpan(
              text: "Search", 
              style: TextStyle(
                color: Color.fromRGBO(204, 23, 40, 1),
                fontSize: 40
              ),
            ),
          ]
        ),
      ),
    },
    {
      'page': AccountScreen(),
      'title': RichText(
        text: TextSpan(
          children: <TextSpan>[
            TextSpan(
              text: "Akun", 
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              )
            ),
          ]
        ),
      ),
    }
  ];

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: _pages[_selectedPageIndex]['title'],
      ),
      backgroundColor: Theme.of(context).primaryColor,
      // drawer: MainDrawer(),
      body: DoubleBackToCloseApp(
        snackBar: SnackBar(
          backgroundColor: Color.fromRGBO(204, 23, 40, 1),
          content: Container(
            height: 25,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                "Tekan kembali lagi untuk keluar",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'Roboto',
                )
              ),
            ),
          ),
          duration: Duration(seconds: 2),
          behavior: SnackBarBehavior.floating,
          width: 250,
          padding: const EdgeInsets.all(5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        child: Container(
          child: _pages[_selectedPageIndex]['page'],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: "Account",
          ),
        ],
        currentIndex: _selectedPageIndex,
        selectedItemColor: Theme.of(context).accentColor,
        onTap: _selectPage,
      ),
    );
  }
}
