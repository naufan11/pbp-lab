import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isPasswordVisible = false;
  void togglePasswordView() {
    setState(() {
      isPasswordVisible = !isPasswordVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: <Widget>[
            SizedBox( height: 60, ),
            Image(
              image: AssetImage('assets/images/logo.png'),
              width: 100,
              height: 100,
            ),
            Spacer(),
            Container(
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(250, 250, 250, 0.95),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Username',
                        hintStyle: TextStyle(
                          color: Color.fromRGBO(200, 200, 200, 1),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none, 
                        ),
                      ),
                    )
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(250, 250, 250, 0.95),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: TextFormField(
                      obscureText: !isPasswordVisible,
                      decoration: InputDecoration(
                        hintText: 'Password',
                        hintStyle: TextStyle(
                          color: Color.fromRGBO(200, 200, 200, 1),
                        ),
                        suffixIcon: IconButton(
                          color: Color.fromRGBO(200, 200, 200, 1),
                          splashRadius: 1,
                          icon: Icon(isPasswordVisible ? 
                            Icons.visibility_outlined : 
                            Icons.visibility_off_outlined
                          ),
                          onPressed: togglePasswordView,
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none, 
                        ),
                      ),
                    )
                  ),
                ],
              ),
            ),
            SizedBox( height: 30, ),
            Container(
              width: double.infinity,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    Color.fromRGBO(204, 23, 40, 1)
                  ),
                  foregroundColor: MaterialStateProperty.all<Color>(
                    Colors.white
                  ),
                  overlayColor: MaterialStateProperty.resolveWith<Color?>(
                    (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed))
                        return Color.fromRGBO(255, 0, 0, 1);
                      return null; // Defer to the widget's default.
                    }
                  ),
                ),
                onPressed: () {},
                child: Text(
                  "Login",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            SizedBox( height: 10, ),
            RichText(
              text: TextSpan(
                text: "Belum memiliki akun?",
                style: TextStyle(
                  color: Colors.black,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.popAndPushNamed(context, "/register");
                  }
              ),
            ),
            SizedBox( height: 220, ),
          ],
        ),
      )
    );
  }
}