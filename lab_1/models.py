from django.db import models
from django.core.validators import MinLengthValidator, int_list_validator

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(verbose_name="Nama", max_length=30)
    # TODO Implement missing attributes in Friend model

    npm = models.CharField(verbose_name="NPM", max_length=10,
                           validators=[int_list_validator(sep=''),
                           MinLengthValidator(10),])

    birth_date = models.DateField(verbose_name="DoB")
