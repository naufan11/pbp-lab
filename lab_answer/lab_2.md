# Lab 2 Answer

## Apakah perbedaan antara JSON dan XML?
|             JSON            |          XML          |
|:---------------------------:|:---------------------:|
|  JavaScript Object Notation |    Markup Language    |
|     data strukturnya map    | data strukturnya tree |
| data dapat langsung diakses |    perlu di-parsing   |
| tidak bisa menampilkan data | bisa menampilkan data |

https://www.geeksforgeeks.org/difference-between-json-and-xml/ <br>
https://blog.cloud-elements.com/json-better-xml <br>
https://www.imaginarycloud.com/blog/json-vs-xml/ <br>
https://www.guru99.com/json-vs-xml-difference.html

<br><br>

## Apakah perbedaan antara HTML dan XML?
|               HTML              |              XML             |
|:-------------------------------:|:----------------------------:|
|    tagnya sudah didefinisikan   |   bisa membuat tag sendiri   |
|      untuk menampilkan data     |    untuk mentransfer data    |
| dapat langsung di-parse oleh JS | perlu XML DOM untuk di-parse |
|              static             |            dynamic           |

https://www.geeksforgeeks.org/html-vs-xml/ <br>
https://www.guru99.com/xml-vs-html-difference.html
