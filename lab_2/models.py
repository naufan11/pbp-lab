from django.db import models

# Create your models here.
class Note(models.Model):
    msgTo = models.CharField(verbose_name="to", max_length=30)
    msgFrom = models.CharField(verbose_name="from", max_length=30)
    msgTitle = models.CharField(verbose_name="title", max_length=50)
    msg = models.CharField(verbose_name="message", max_length=100)