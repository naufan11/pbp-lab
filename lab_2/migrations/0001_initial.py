# Generated by Django 3.2.7 on 2021-09-21 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('msgTo', models.CharField(max_length=30, verbose_name='to')),
                ('msgFrom', models.CharField(max_length=30, verbose_name='from')),
                ('msgTitle', models.CharField(max_length=50, verbose_name='title')),
                ('msg', models.CharField(max_length=100, verbose_name='message')),
            ],
        ),
    ]
