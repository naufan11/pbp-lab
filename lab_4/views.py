from django.shortcuts import redirect, render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)

def add_note(request):
    if (request.method == "POST"):
        form = NoteForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/lab-4')
            
    else:
        form = NoteForm()

    return render(request, "lab4_form.html", {"form" : form})