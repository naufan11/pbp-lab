from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    data = serializers.serialize('json', [Note.objects.get(id = id),])
    return HttpResponse(data, content_type="application/json")


def update_note(request, id):
    note = get_object_or_404(Note, id = id)
    if request.is_ajax and request.method == "POST":
        form = NoteForm(request.POST, instance=note)
        if form.is_valid():
            inst = form.save()
            inst_data = serializers.serialize('json', [inst,])
            return JsonResponse({"instance": inst_data}, status=200)
        else:
            return JsonResponse({"error": form.errors}, status=400)
    
    return JsonResponse({"error": ""}, status=400)


def delete_note(request, id):
    note = get_object_or_404(Note, id = id)

    if request.method == "POST":
        note.delete()
        note_data = serializers.serialize('json', [note,])
        return JsonResponse({"note": note_data}, status=200)
    
    return JsonResponse({"error": ""}, status=400)