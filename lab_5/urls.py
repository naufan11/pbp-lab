from django.urls import path
from .views import index, get_note, update_note, delete_note

urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>', get_note),
    path('notes/<id>/update', update_note),
    path('notes/<id>/delete', delete_note),
]
