$(document).ready(function(){
    $(".btn-v").click(function(){
        var id = $(this).closest("tr").attr("id");

        $.ajax({
            url: "notes/" + id,
            dataType: 'json',
            success: function(result){
                var fields    = result[0].fields;
                var toHtml    = '<tr><td class="modal-table">To</td><td class="text-break">' + fields['msgTo'] + '</td></tr>'
                var fromHtml  = '<tr><td class="modal-table">From</td><td class="text-break">' + fields['msgFrom'] + '</td></tr>'
                var titleHtml = '<tr><td class="modal-table">Title</td><td class="text-break">' + fields['msgTitle'] + '</td></tr>'
                var msgHtml   = '<tr><td class="modal-table">Message</td><td class="text-break">' + fields['msg'] + '</td></tr>'

                var tableHtml = '<table class="table table-borderless">' + 
                                fromHtml + toHtml + titleHtml + msgHtml + 
                                '</table>'
                $("#viewModal .modal-body").html(tableHtml);

                $("#viewModalLabel").text("Note " + id);
            }
        });
    });
    
    $(".btn-e").click(function(){
        var id = $(this).closest("tr").attr("id");

        $.ajax({
            url: "notes/" + id,
            dataType: 'json',
            success: function(result){
                var fields = result[0].fields;
                var to    = fields['msgTo'];
                var from  = fields['msgFrom'];
                var title = fields['msgTitle'];
                var msg   = fields['msg'];

                $("#editModal #floatingTo").attr("value", to);
                $("#editModal #floatingFrom").attr("value", from);
                $("#editModal #floatingTitle").attr("value", title);
                $("#editModal #floatingMessage").html(msg);
                $("#editModal .btn-save").attr("value", id);

                $("#editModalLabel").text("Edit Note " + id);
            }
        });
    });

    $(".btn-save").click(function(){
        var id = $(this).attr("value");
        var csrftoken = Cookies.get('csrftoken');
        $.ajax({
            url: "notes/" + id + "/update",
            type: "POST",
            data: $("#modal-update").serialize(),
            success: function(response){
                var instance = JSON.parse(response["instance"]);
                var fields = instance[0].fields;
                var to     = fields['msgTo'];
                var from   = fields['msgFrom'];
                var title  = fields['msgTitle'];
                var msg    = fields['msg'];

                $("#" + id + " .td-to").text(to);
                $("#" + id + " .td-from").text(from);
                $("#" + id + " .td-title").text(title);
                $("#" + id + " .td-msg").html(msg);

                $("#modal-update").get(0).reset();
            }
        });
    });

    $(".btn-d").click(function(){
        var id = $(this).closest("tr").attr("id");

        $.ajax({
            url: "notes/" + id,
            dataType: 'json',
            success: function(result){
                var fields    = result[0].fields;
                var toHtml    = '<tr><td class="modal-table">To</td><td class="text-break">' + fields['msgTo'] + '</td></tr>'
                var fromHtml  = '<tr><td class="modal-table">From</td><td class="text-break">' + fields['msgFrom'] + '</td></tr>'
                var titleHtml = '<tr><td class="modal-table">Title</td><td class="text-break">' + fields['msgTitle'] + '</td></tr>'
                var msgHtml   = '<tr><td class="modal-table">Message</td><td class="text-break">' + fields['msg'] + '</td></tr>'

                var tableHtml = '<table class="table table-borderless">' + 
                                fromHtml + toHtml + titleHtml + msgHtml + 
                                '</table>'
                $("#deleteModal .modal-body").html(tableHtml + "<p>Do you want to delete this note?</p>");

                $("#deleteModal .btn-del").attr("value", id);
                $("#deleteModalLabel").text("Delete Note " + id);
            }
        });
    });

    $(".btn-del").click(function(){
        var id = $(this).attr("value");
        var csrftoken = Cookies.get('csrftoken');
        $.ajax({
            url: "notes/" + id + "/delete",
            type: "POST",
            data: { csrfmiddlewaretoken: csrftoken },
            success: function(response){
                $("#" + id).remove();
            }
        });
    });
})